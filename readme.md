#Fortune Teller

##Requirements
[Bootstrap](https://getbootstrap.com/docs/4.3/getting-started/introduction/)

##Setup
Download the source code.

Open `index.html` in your web browser.

Optionally, you can use Python's SimpleHTTPServer:

Command line: cd into the top directory of the source code.

For Python 2.7 use: `python2.7 -m SimpleHTTPServer 9000`

For Python 3 use: `python3 -m http.server 9000`