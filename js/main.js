var fortuneApp = {
    // model level
    model: {
        targets: {},
        proverbs: [],
        userName: "",
        userYOB: ""
    },
    // controller level
    controller: {
        getFortune: function (name, yob, proverbs) {
            "use strict";

            // Uses the length of the user name and the
            // year of birth to get an index. If the index
            // is longer than the length of the proverbs
            // list, just take a random index, because your
            // fortune, life, everything, is random, after all.
            // Takes in the name <str>, yob <str>, and proverbs <array>.
            // Returns the fortune <str>.

            var nameIndex = Math.floor(yob / name.length);
            var provLen = proverbs.length - 1;
            var provIndex = Math.floor(Math.random() * nameIndex);
            var fortune = proverbs[Math.floor(Math.random() * provLen)]; // default

            if (provIndex <= provLen) {
                fortune = proverbs[provIndex];
            }

            return fortune;
        },
        getUrlParams: function (urlString) {
            "use strict";

            // Gets the name and yob from the url string
            // and verifies a year (length = 4). Takes in
            // the current url params <str>. Returns the
            // params <array>.

            var userUrl = new URL(urlString);
            var userName = userUrl.searchParams.get("yourName");
            var userYOB = userUrl.searchParams.get("yourYOB");
            var userYobLen = userYOB.length;
            var userYobDif = 4 - userYobLen;
            var index = 0;
            var params = {name: null, yob: null};

            if (userYobDif !== 0) {
                // add some zeros to complete the value
                while (index < userYobDif) {
                    userYOB += "0";
                    index += 1;
                }
            }

            params.name = userName;
            params.yob = userYOB;

            return params;
        },
        reset: function (targets) {
            targets.yourFortune.style.display = "none";
            targets.yourFortune.innerHTML = "&nbsp;";
            targets.resetBtn.style.display = "none";
        },
        init: function () {
            "use strict";
            // This is the main controller function.

            var url = window.location.href;
            var urlParams = null;
            var fortune = null;

            if (url.indexOf("?") > -1) {
                // get form input
                urlParams = fortuneApp.controller.getUrlParams(url);
                // write to the model
                fortuneApp.model.userName = urlParams.name;
                fortuneApp.model.userYOB = urlParams.yob;

                // get the fortune
                fortune = fortuneApp.controller.getFortune(fortuneApp.model.userName, fortuneApp.model.userYOB, fortuneApp.model.proverbs);

                // render the view
                fortuneApp.view.render(fortuneApp.model.targets, fortune);

                // listen for a reset
                fortuneApp.model.targets.resetBtn.addEventListener("click", function () {
                    fortuneApp.controller.reset(fortuneApp.model.targets);
                });
            }
        }
    },
    // view level
    view: {
        render: function (targets, text) {
            targets.yourFortune.style.display = "block";
            targets.yourFortune.innerHTML = text;
            targets.resetBtn.style.display = "inline";

            // for reference and testing
            console.log(fortuneApp.model);
        }
    }
};